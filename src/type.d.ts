export interface IPeopledata {
    count:number;
    next:string | null;
    previous?:string | null;
    results:Result[]

}

export type Result = {
    name:string,
    height:string,
    mass:string,
    gender:string,
    homeworld:string
}


