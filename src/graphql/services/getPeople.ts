import axios from "axios";
import { IPeopledata } from "../../type";

export default async function getPeople(root:undefined,{page,searchQuery}: {
    page:number,
    searchQuery:string
}): Promise<IPeopledata | {
    message: string
}>{
    try {
        let url ;

        if (!searchQuery){
            url = `https://swapi.dev/api/people/?page=${page || 1}`
        }else{
            url = `https://swapi.dev/api/people/?page=${page || 1}&search=${searchQuery || ""}`;
        }
        const response = await axios.get(url);
        console.log(response) ;
        return response.data;
    }
    catch(e: any) {
        throw {
            message: e.message
        }
    }
}
